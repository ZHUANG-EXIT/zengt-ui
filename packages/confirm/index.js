﻿import Vue from 'vue';
import VueConfirm from './Confirm';
import { isServer } from '../utils';
let instance;

const initInstance = () => {
  console.log('初始化');
  instance = new (Vue.extend(VueConfirm))({
    el: document.createElement('div')
  });

  instance.$on('input', value => {
    instance.value = value;
  });

  document.body.appendChild(instance.$el);
};

const Confirm = options => {
  /* istanbul ignore if */
  if (isServer) return;

  if (!instance) {
    initInstance();
  }
  /*
  if (typeof Promise !== 'undefined') {
    return new Promise((resolve, reject) => {
      if (!instance) {
        initInstance();
      }

      Object.assign(instance, Confirm.currentOptions, options, {
        resolve,
        reject
      });
    });
  }
  else {
    if (!instance) {
      initInstance();
    }

    Object.assign(instance, Confirm.currentOptions, options, {
      resolve,
      reject
    });
  };
  */
};

Confirm.defaultOptions = {
  value: true,
  title: '',
  message: '',
  overlay: true,
  className: '',
  lockScroll: true,
  beforeClose: null,
  messageAlign: '',
  confirmButtonText: '',
  cancelButtonText: '',
  showConfirmButton: true,
  showCancelButton: false,
  closeOnClickOverlay: false,
  callback: action => {
    instance[action === 'confirm' ? 'resolve' : 'reject'](action);
  }
};

Confirm.alert = Confirm;

Confirm.confirm = options => Confirm({
  showCancelButton: true,
  ...options
});

Confirm.close = () => {
  if (instance) {
    instance.value = false;
  }
};

Confirm.setDefaultOptions = options => {
  Object.assign(Confirm.currentOptions, options);
};

Confirm.resetDefaultOptions = () => {
  Confirm.currentOptions = { ...Confirm.defaultOptions };
};

Confirm.install = () => {
  Vue.use(VueConfirm);
};

Vue.prototype.$Confirm = Confirm;
Confirm.resetDefaultOptions();

export default Confirm;
